# Django Password Authentication
Most websites have a user management system. Using the Django authentication system saves you all the hustle and bustle of doing your own authentication.


Code example used in the repository can be shipped out in any project.

Lets Communicate on my [Twitter](https://twitter.com/Brianjoroge) handle

## Running the Project Locally

First, clone the repository to your local machine:

```bash
git clone https://BriaNjoroge@bitbucket.org/BriaNjoroge/django-user-authentication.git
```

Install the requirements:

```bash
pip install -r requirements.txt
```

Apply the migrations:

```bash
python manage.py migrate
```

Finally, run the development server:

```bash
python manage.py runserver
```

The project will be available at **127.0.0.1:8000**.


## License

The source code is released under the [MIT License](https://github.com/sibtc/django-auth-tutorial-example/blob/master/LICENSE).
